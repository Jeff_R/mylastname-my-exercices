export const my_is_posi_neg = (nbr) => {
    
  let result = "";

  if (nbr == undefined || nbr == null) 
  {
    result = "POSITIF";
  }

  else if (nbr == 0) 
  {
    result = "NEUTRAL";
  }

  else 
  {
    let toStr = nbr.toString();

    if (toStr[0] == "-") 
    {
      result = "NEGATIF";
    }

    else 
    {
      result = "POSITIF";
    }
  }

  return result;
}
  

