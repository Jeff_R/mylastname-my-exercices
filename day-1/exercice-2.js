export const my_display_alpha_reverse = () => {
    
    let alpha_reverse = " ";
    for ( let i = 97; i < 123; i++)
    {
        let str = String.fromCharCode(i);
        alpha_reverse = str + alpha_reverse;
    }

    return alpha_reverse;
    
}
